﻿using System;
using System.Collections.Generic;
using static System.Console;
using System.Data.SQLite;
using System.Data;

namespace TestTask
{
    public class CenterDAL
    {
        SQLiteConnection _connection;

        public CenterDAL(string connStr)
        {
            try
            {
                string connectionString = connStr;
                _connection = new SQLiteConnection(connectionString);
            }
            catch(Exception e)
            {
                WriteLine("\n***Ошибка!***");
                WriteLine($"Метод: {e.TargetSite}");
                WriteLine($"Сообщение: {e.Message}");
                WriteLine($"Источник ошибки: {e.Source}");
            }
        }

        public void Open()
        {
            _connection.Open();
        }

        public void Close()
        {
            _connection.Close();
        }

        public DataTable Select(List<string> col, string tn)
        {
            DataTable dt = new DataTable();

            string columns = "";

            for (int i = 0; i < col.Count - 2; i++)
            {
                columns += $"{col[i]}, ";
            }

            columns += col[col.Count - 1];

            string query = $"select {columns} from {tn}";

            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(query, _connection);

            dataAdapter.Fill(dt);

            dataAdapter.Dispose();

            return dt;
        }

        public void Delete(string tn, int firstId, int lastId)
        {
            string query = $"delete from {tn} where {tn}.id between {firstId} and {lastId}";

            SQLiteCommand command = new SQLiteCommand(query, _connection);
            command.ExecuteNonQuery();
        }
    }
}
