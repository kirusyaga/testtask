﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data;

namespace TestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument settingsDoc = XDocument.Load("settings.xml");
            IEnumerable<XElement> allElements = settingsDoc.Descendants();

            string connectionString = "";
            string targetFolder = "";
            bool deleteAfter = false;
            string tableName = "";
            List<string> columns = new List<string>();

            foreach (XElement item in allElements)
            {
                switch (item.Name.ToString())
                {
                    default:
                        break;

                    case "connectionString":
                        connectionString = $"{item.Value}";
                        break;

                    case "targetFolder":
                        targetFolder = $"{item.Value}";
                        break;

                    case "tableSettings":
                        if (item.FirstAttribute.Name.ToString() == "deleteAfter")
                        {
                            if (item.FirstAttribute.Value == "true")
                            {
                                deleteAfter = true;
                            }
                            else
                            {
                                deleteAfter = false;
                            }
                        }
                        break;

                    case "tableName":
                        tableName = $"{item.Value}";
                        break;

                    case "column":
                        columns.Add(item.Value);
                        break;
                }
            }

            if(!columns.Contains("id"))
            {
                columns.Add("id");
            }

            CenterDAL centerDAL = new CenterDAL(connectionString);
            centerDAL.Open();

            DataTable dataTable = centerDAL.Select(columns, tableName);
            dataTable.TableName = tableName;
            DataColumn[] keyColumns = new DataColumn[1];
            keyColumns[0] = dataTable.Columns["id"];
            dataTable.PrimaryKey = keyColumns;

            if (!Directory.Exists(targetFolder))
            {
                Directory.CreateDirectory(targetFolder);
            }

            int rowsCount = dataTable.DefaultView.Table.Rows.Count;
            string fileName = "";
            DataTable temp = new DataTable();
            temp.TableName = "Temp";

            for(int i = 0; i < dataTable.Columns.Count; i++)
            {
                temp.Columns.Add();
                temp.Columns[i].ColumnName = dataTable.Columns[i].ColumnName;
                temp.Columns[i].DataType = dataTable.Columns[i].DataType;
            }

            keyColumns[0] = temp.Columns["id"];
            temp.PrimaryKey = keyColumns;

            for (int i = 1; i <= rowsCount; i++)
            {
                if (i % 10 == 0)
                {
                    DataRow row = dataTable.Rows.Find(i);
                    temp.ImportRow(row);

                    fileName = $"{targetFolder}\\{tableName}_from_{i - 9}_to_{i}.xml";
                    FileStream file = new FileStream(fileName, FileMode.Create);
                    file.Close();
                    temp.WriteXml(fileName);
                    temp.Rows.Clear();

                    if (deleteAfter)
                    {
                        centerDAL.Delete(tableName,i - 9, i);
                    }
                }
                else
                {
                    DataRow row = dataTable.Rows.Find(i);
                    temp.ImportRow(row);

                    if (rowsCount % 10 != 0 && i == rowsCount)
                    {
                        fileName = $"{targetFolder}\\{tableName}_from_{((i / 10) * 10) + 1}_to_{i}.xml";
                        FileStream file = new FileStream(fileName, FileMode.Create);
                        file.Close();
                        temp.WriteXml(fileName);
                        temp.Rows.Clear();

                        if (deleteAfter)
                        {
                            centerDAL.Delete(tableName, (i / 10) * 10 + 1, i);
                        }
                    }
                }
            }
        }
    }
}